// User - place order, receive food from driver
// Restaurant - cook food, pack food for delivery
// Driver - pick up food from resto, deliver food to user

class Order {
  var name: String

  init(name: String) {
    self.name = name
  }
}

protocol MakeOrder {
  func make(order: Order)
}

protocol PickUpOrder {
  func deliver(order: Order)
}

class User {
  var userDelegate: MakeOrder?
  var name: String = "User"

  // User's responsibility to order, but not make the food
  func place (order: Order) {
    print("\(name): Placed an order for \(order.name)")

    // Making order transferred to restaurant
    userDelegate?.make(order: order)
  }

  func receive () {
    print("\(name): Yay! I received my order!")
  }
}

class Restaurant: MakeOrder {
  var restaurantDelegate: PickUpOrder?
  var name: String = "Restaurant"

  // Restaurant's responsibility to make food, but not deliver
  func make(order: Order) {
    print("\(name): Received order \(order.name)...")
    print("\(name): Cooking \(order.name)...")
  }

  func deliverOrder() {
    // Delivering order is transferred to driver
    restaurantDelegate?.deliver(order: order)
  }
}

class Driver: PickUpOrder {
  var name: String = "Driver"

  func deliver(order: Order) {
    print("\(name): Picked up order \(order.name)")
    print("\(name): On my way to user!")
  }
}

let order = Order(name: "Cheeseburger Meal")

let user = User()
user.userDelegate = Restaurant()
user.place(order: order)

let resto = Restaurant()
resto.restaurantDelegate = Driver()
resto.deliverOrder()

user.receive()

